from rest_framework import serializers
from clientes_api.models import Cliente

class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ["email", "cpf"]
