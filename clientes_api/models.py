from django.db import models

# Create your models here.
class Cliente(models.Model):
    email = models.CharField(max_length=30)
    cpf = models.CharField(max_length=11)

    def __str__(self):
        return self.email
