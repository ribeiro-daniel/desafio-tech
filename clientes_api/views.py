from rest_framework import viewsets
from clientes_api.models import Cliente
from clientes_api.serializer import ClienteSerializer

class ClientesViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
