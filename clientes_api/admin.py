from django.contrib import admin
from clientes_api.models import Cliente

# Register your models here.
class Clientes(admin.ModelAdmin):
    list_display = ("id", "email", "cpf")
    list_display_links = ("id", "email")
    search_fields = ("email",)

admin.site.register(Cliente, Clientes)
