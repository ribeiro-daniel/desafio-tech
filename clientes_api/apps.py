from django.apps import AppConfig


class ClientesApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'clientes_api'
